syntax on
set encoding=utf-8
set autoindent
set autoread
set autowrite
set backspace=indent,eol,start
set history=100
set hlsearch
set incsearch
set noerrorbells
set nowrap
set shiftwidth=4
set hidden
set visualbell
set ttimeoutlen=50
set noshowmode "Hide mode text
set showbreak=↪\



"""""""""""""""""""""""""""""""
"" Airline-vim
"""""""""""""""""""""""""""""""
set laststatus=2
let g:airline_left_sep=''
let g:airline_right_sep=''
let g:airline_powerline_fonts=1
let g:airline_theme='dark'

filetype plugin indent on


"""""""""""""""""""""""""""""""
"" Pathogen
"""""""""""""""""""""""""""""""
execute pathogen#infect()

"""""""""""""""""""""""""""
"" General look
"""""""""""""""""""""""""""
set background=dark


